unit TextVar;

interface

uses
  SysUtils, Classes;

type

  TCustomTextVar = class(TComponent)
  private
    { Private declarations }
    fLines: TStringList;
    FReadOnly: Boolean;
    procedure SetReadOnly(const Value: Boolean);
    function GetText: string;
    procedure SetText(const Value: string);
  protected
    function getLines: TStrings;
    procedure SetLines(const Value: TStrings);
  public
    { Public declarations }
    function Format(args: array of const): string;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    property Text: string read GetText write SetText;
    property Lines: TStrings read getLines write SetLines;
  published
    { Published declarations }
    property ReadOnly: Boolean read FReadOnly write SetReadOnly default False;
  end;

  TSqlVar = class (TCustomTextVar)
  published
    property SQL: TStrings read getLines write SetLines;
  end;

  TTextVar = class (TCustomTextVar)
  published
    property Lines;
  end;

implementation
uses Windows;

{ TCustomTextVar }

constructor TCustomTextVar.Create(AOwner: TComponent);
begin
  inherited;
  fLines := TStringList.Create;
end;

destructor TCustomTextVar.Destroy;
begin
  FreeAndNil(fLines);
  inherited;
end;

function TCustomTextVar.getLines: TStrings;
begin
  Result := fLines;
end;

procedure TCustomTextVar.SetReadOnly(const Value: Boolean);
begin
  FReadOnly := Value;
end;

procedure TCustomTextVar.SetLines(const Value: TStrings);
begin
  fLines.Assign(Value);
end;

function TCustomTextVar.GetText: string;
begin
  Result := fLines.Text;
end;

procedure TCustomTextVar.SetText(const Value: string);
begin
  if not fReadOnly then
    fLines.Text := Value
  else
    Raise Exception.CreateFmt('%s: Component text is marked as ReadOnly (%s)',
      [self.ClassName, self.Name]);
end;

function TCustomTextVar.Format(args: array of const): string;
begin
  Result := SysUtils.Format(fLines.Text, args);
end;

end.
