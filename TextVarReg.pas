unit TextVarReg;

interface
uses Windows, Graphics, Forms, Controls, Buttons, StdCtrls, ExtCtrls, ComCtrls,
  DesignIntf, DesignEditors, TextVar, Classes, Dialogs, Menus, ActnPopup;

type
  TCustomTextVarEditor = class(TComponentEditor)
  protected
    Procedure EditTStringsProperty(TheProperty: TStrings; PropertyName: string);
  public
    procedure ExecuteVerb(Index: Integer); override;
    function GetVerbCount: Integer; override;
    function GetVerb(Index: Integer): string; override;
  end;

  TSqlVarEditor = class(TCustomTextVarEditor)
  public
    procedure Edit; override;
  end;

  TTextVarEditor = class(TCustomTextVarEditor)
  public
    procedure Edit; override;
  end;

procedure Register;

implementation
uses ActiveX, SysUtils, DesignConst, ToolsAPI, IStreams,
  StFilSys, TypInfo, StringsEdit, ValueEdit;

type
  TStringsModuleCreator = class(TInterfacedObject, IOTACreator, IOTAModuleCreator)
  private
    FFileName: string;
    FStream: TStringStream;
    FAge: TDateTime;
  public
    constructor Create(const FileName: string; Stream: TStringStream; Age: TDateTime);
    destructor Destroy; override;
    { IOTACreator }
    function GetCreatorType: string;
    function GetExisting: Boolean;
    function GetFileSystem: string;
    function GetOwner: IOTAModule;
    function GetUnnamed: Boolean;
    { IOTAModuleCreator }
    function GetAncestorName: string;
    function GetImplFileName: string;
    function GetIntfFileName: string;
    function GetFormName: string;
    function GetMainForm: Boolean;
    function GetShowForm: Boolean;
    function GetShowSource: Boolean;
    function NewFormFile(const FormIdent, AncestorIdent: string): IOTAFile;
    function NewImplSource(const ModuleIdent, FormIdent, AncestorIdent: string): IOTAFile;
    function NewIntfSource(const ModuleIdent, FormIdent, AncestorIdent: string): IOTAFile;
    procedure FormCreated(const FormEditor: IOTAFormEditor);
  end;
  TOTAFile = class(TInterfacedObject, IOTAFile)
  private
    FSource: string;
    FAge: TDateTime;
  public
    constructor Create(const ASource: string; AAge: TDateTime);
    { IOTAFile }
    function GetSource: string;
    function GetAge: TDateTime;
  end;

{ TOTAFile }

constructor TOTAFile.Create(const ASource: string; AAge: TDateTime);
begin
  inherited Create;
  FSource := ASource;
  FAge := AAge;
end;

function TOTAFile.GetAge: TDateTime;
begin
  Result := FAge;
end;

function TOTAFile.GetSource: string;
begin
  Result := FSource;
end;

{ TStringsModuleCreator }

constructor TStringsModuleCreator.Create(const FileName: string; Stream: TStringStream;
  Age: TDateTime);
begin
  inherited Create;
  FFileName := FileName;
  FStream := Stream;
  FAge := Age;
end;

destructor TStringsModuleCreator.Destroy;
begin
  FStream.Free;
  inherited;
end;

procedure TStringsModuleCreator.FormCreated(const FormEditor: IOTAFormEditor);
begin
  { Nothing to do }
end;

function TStringsModuleCreator.GetAncestorName: string;
begin
  Result := '';
end;

function TStringsModuleCreator.GetCreatorType: string;
begin
  Result := sText;
end;

function TStringsModuleCreator.GetExisting: Boolean;
begin
  Result := True;
end;

function TStringsModuleCreator.GetFileSystem: string;
begin
  Result := sTStringsFileSystem;
end;

function TStringsModuleCreator.GetFormName: string;
begin
  Result := '';
end;

function TStringsModuleCreator.GetImplFileName: string;
begin
  Result := FFileName;
end;

function TStringsModuleCreator.GetIntfFileName: string;
begin
  Result := '';
end;

function TStringsModuleCreator.GetMainForm: Boolean;
begin
  Result := False;
end;

function TStringsModuleCreator.GetOwner: IOTAModule;
begin
  Result := nil;
end;

function TStringsModuleCreator.GetShowForm: Boolean;
begin
  Result := False;
end;

function TStringsModuleCreator.GetShowSource: Boolean;
begin
  Result := True;
end;

function TStringsModuleCreator.GetUnnamed: Boolean;
begin
  Result := False;
end;

function TStringsModuleCreator.NewFormFile(const FormIdent,
  AncestorIdent: string): IOTAFile;
begin
  Result := nil;
end;

function TStringsModuleCreator.NewImplSource(const ModuleIdent, FormIdent,
  AncestorIdent: string): IOTAFile;
begin
  Result := TOTAFile.Create(FStream.DataString, FAge);
end;

function TStringsModuleCreator.NewIntfSource(const ModuleIdent, FormIdent,
  AncestorIdent: string): IOTAFile;
begin
  Result := nil;
end;

procedure Register;
begin
  RegisterComponents('danik_ik', [TTextVar]);
  RegisterComponents('danik_ik', [TSQLVar]);
  RegisterComponentEditor(TTextVar, TTextVarEditor);
  RegisterComponentEditor(TSQLVar, TSqlVarEditor);
end;


{ TCustomTextVarEditor }

procedure TCustomTextVarEditor.EditTStringsProperty(TheProperty: TStrings; PropertyName: string);
var
  Ident: string;
  Component: TComponent;
  Module: IOTAModule;
  Editor: IOTAEditor;
  ModuleServices: IOTAModuleServices;
  Stream: TStringStream;
  Age: TDateTime;
begin
  Component := TComponent(GetComponent);
  ModuleServices := BorlandIDEServices as IOTAModuleServices;
  if (TObject(Component) is TComponent) and
    (Component.Owner = Self.Designer.GetRoot) and
    (Self.Designer.GetRoot.Name <> '') then
  begin
    Ident := Self.Designer.GetRoot.Name + DotSep +
      Component.Name + DotSep + PropertyName;
    Module := ModuleServices.FindModule(Ident);
  end else Module := nil;
  if (Module <> nil) and (Module.GetModuleFileCount > 0) then
    Module.GetModuleFileEditor(0).Show
  else
  begin
    StFilSys.Register;
    Stream := TStringStream.Create('');
    TheProperty.SaveToStream(Stream);
    Stream.Position := 0;
    Age := Now;
    Module := ModuleServices.CreateModule(
      TStringsModuleCreator.Create(Ident, Stream, Age));
    if Module <> nil then
    begin
      with StringsFileSystem.GetTStringsProperty(Ident, Component, PropertyName) do
        DiskAge := DateTimeToFileDate(Age);
      Editor := Module.GetModuleFileEditor(0);
      Editor.Show;
    end;
  end;
end;

procedure TCustomTextVarEditor.ExecuteVerb(Index: Integer);
begin
   inherited;
   case Index of
      0: ShowMessage('Demo menu called');
   end; 
end;

function TCustomTextVarEditor.GetVerb(Index: Integer): string;
begin
   case Index of
      0: Result := 'Demo Menu Item 1';
   end; 
end;

function TCustomTextVarEditor.GetVerbCount: Integer;
begin
   Result := 1; 
end;

// TODO -o Danila: ������� �������� ��� TTextCollection

{ TSqlVarEditor }

procedure TSqlVarEditor.Edit;
begin
  EditTStringsProperty(TSqlVar(GetComponent).SQL, 'SQL');
end;

{ TTextVarEditor }

procedure TTextVarEditor.Edit;
begin
  EditTStringsProperty(TTextVar(GetComponent).Lines, 'Lines');
end;

end.
